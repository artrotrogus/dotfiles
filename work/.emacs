(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; add MELPA package server
(require 'package)
(add-to-list 'package-archives
	         '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
;; if not yet installed, install package use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure 't)

;; Custom Lisp Code
(add-to-list 'load-path "~/.emacs.d/lisp/")
(load "~/.emacs.d/lisp/my-abbrev.el")

;; Backup Policy
(setq make-backup-files nil)  ;; to have totally no backups
(setq create-lockfiles nil)  ;; to have totally no lockfiles
(setq auto-save-default nil)  ;; autosave off
(defalias 'yes-or-no-p 'y-or-n-p)

;; Appearance
(column-number-mode)
(setq show-paren-delay 0)
(show-paren-mode)
(setq-default show-trailing-whitespace t) ;; Show stray whitespace.
(setq-default indicate-buffer-boundaries 'left)
(global-set-key (kbd "C-c d") 'delete-trailing-whitespace)
(setq sentence-end-double-space nil)
(setq-default indent-tabs-mode nil) ;; Use spaces, not tabs, for indentation.
(setq-default tab-width 4)
(global-visual-line-mode 1) ;; Visual wrap
(if (display-graphic-p)
    (setq initial-frame-alist
          '(
            (background-color . "#EBE8D9")
            (left . 50)
            (top . 50)))
  (setq initial-frame-alist '( (tool-bar-lines . 0))))
(setq default-frame-alist initial-frame-alist)

;; Use EVIL layer
(setq evil-want-abbrev-expand-on-insert-exit nil)
(use-package evil
  :ensure t
  :init
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1))
(evil-define-command evil-tab-sensitive-quit (&optional bang)
  :repeat nil
  (interactive "<!>")
  (if (> (length (elscreen-get-screen-list)) 1)
      (if (> (count-window) 1)
          (evil-quit)
        (elscreen-kill))
    (evil-quit bang)))
(use-package evil-numbers
  :ensure t
  :bind (:map evil-normal-state-map
	      ("C-a" . evil-numbers/inc-at-pt)))
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))
(use-package undo-tree
  :ensure t
  :config
    (global-undo-tree-mode)
    (evil-set-undo-system 'undo-tree))

;; simpleclip to get elements from clipboard
(use-package simpleclip
  :ensure t
  :config
  (simpleclip-mode 1))

;; SMEX
(global-set-key (kbd "M-x") 'smex)

;; Magit
(use-package magit
  :ensure t
  :defer t)

;; Clojure stuff
(use-package cider
  :ensure t)

;; set org variables befor requiring org
(setq line-spacing nil)
(setq org-hide-emphasis-markers t)
(plist-put org-format-latex-options :scale 1.6)

;; Fix parens mismatch when editing code in Org
(defun org-syntax-table-modify ()
  "Modify `org-mode-syntax-table' for the current org buffer."
  (modify-syntax-entry ?< "." org-mode-syntax-table)
  (modify-syntax-entry ?> "." org-mode-syntax-table))
(add-hook 'org-mode-hook #'org-syntax-table-modify)

;; GTD stuff
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(setq org-log-into-drawer t)
(setq org-log-reschedule 'time)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "<f6>") 'org-capture)

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d@/!)")
	      (sequence "WAITING(w@/!)" "IN-PROGRESS(p@/!)" "|" "CANCELED(c@/!)"))))

;; Agenda command definitions
(setq mz/org-agenda-directory "C:/Users/utente/Nextcloud/gtd/")
(setq mz/org-capture-templates (concat mz/org-agenda-directory "capture-templates/"))

(setq org-agenda-files (list (concat mz/org-agenda-directory "inbox.org")
			     (concat mz/org-agenda-directory "gtd.org")
			     (concat mz/org-agenda-directory "tickler.org")))
;; Capture templates
(setq org-capture-templates
      (quote (("i" "Todo [inbox]" entry (file+headline "C:/Users/utente/Nextcloud/gtd/inbox.org" "Tasks")
	           "* TODO %i %?")
	          ("t" "Tickler" entry (file "C:/Users/utente/Nextcloud/gtd/tickler.org")
	           "* %i%?\n  :LOGBOOK:\n  - Added: %U\n  :END:")
	          ("j" "Journal" entry (file+olp+datetree "C:/Users/utente/Nextcloud/gtd/journal.org")
	           "* %U - %^{Activity}")
	          ("o" "New Order" entry (file+headline "C:/Users/utente/Nextcloud/gtd/GTD.org" "Orders")
	           (file "C:/Users/utente/Nextcloud/gtd/capture-templates/order.txt"))
              )))

(defun air-org-skip-subtree-if-priority (priority)
  (let ((subtree-end (save-excursion (org-end-of-subtree t)))
	(pri-value (* 1000 (- org-lowest-priority priority)))
	(pri-current (org-get-priority (thing-at-point 'line t))))
    (if (= pri-value pri-current)
	subtree-end
      nil)))

(setq org-agenda-custom-commands
      '(("d" "Daily agenda and all TODOs"
         ((tags "PRIORITY=\"A\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("DONE" "CANCELED")))
                 (org-agenda-overriding-header "High-priority unfinished tasks:")))
          (agenda "" ((org-agenda-span 3)))
          (alltodo ""
                   ((org-agenda-skip-function '(or (air-org-skip-subtree-if-priority ?A)
                                                   (org-agenda-skip-if nil '(scheduled deadline))))
                    (org-agenda-overriding-header "ALL normal priority tasks:")))))))
;; Refile
(setq org-refile-targets (quote ((nil :maxlevel . 9)
				                 (org-agenda-files :maxlevel . 3)
				                 ("C:/Users/utente/Nextcloud/gtd/someday.org" :level . 1))))
(setq org-refile-use-outline-path (quote file))
(setq org-outline-path-complete-in-steps nil)  ;; Helm completion
(setq org-refile-allow-creating-parent-nodes (quote confirm))

;(plist-put org-format-latex-options :scale 1.6)
(setq org-tags-column 0)
(setq org-src-fontify-natively t) ;; Syntax highlight in #+BEGIN_SRC blocks
(setq org-confirm-babel-evaluate nil) ;; Don't prompt before running code in org
(require 'org)

;; Pretty formatting markup
(use-package org-appear
  :config
  (add-hook 'org-mode-hook 'org-appear-mode))

(use-package olivetti
  :ensure t
  :hook
  (text-mode . olivetti-mode)
  :init
  (setq olivetti-body-width 100)
  :config
  (global-set-key (kbd "<f4>") 'olivetti-mode))

;; Markdown export
(require 'ox-md nil t)

;; Org Babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (python . t)
   ;;(ipython . t)
   (shell . t)
   (scheme . t)
   (racket . t)
   (clojure . t)
   ))
;; Integration
(require 'ob-python)
;;(require 'ob-ipython)
(require 'ob-racket)
(require 'ob-clojure)
(setq org-babel-clojure-backend 'cider)

;; Don't prompt before running code in org
(setq org-confirm-babel-evaluate nil)

; ; Selectrum
(use-package selectrum
  :ensure t
  :config
  (selectrum-mode 1)
  (selectrum-prescient-mode 1)
  (prescient-persist-mode 1))

;;;; Smartparens alternative for Evil layer for editing Lisps
;;(use-package evil-cleverparens
;;  :ensure t
;;  :init
;;  (add-hook 'clojure-mode-hook #'evil-cleverparens-mode)
;;  (add-hook 'racket-mode-hook #'evil-cleverparens-mode))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "outline" :slant normal :weight normal :height 113 :width normal)))))
